///////////////////////////////////////////////////////////////////////////////////////////
var express = require("express");
var fs = require("fs");
var morgan = require("morgan");
var bodyParser = require("body-parser");
var session = require("express-session");
var app = express();
///////////////////////////////////////////////////////////////////////////////////////////
app.use(morgan("tiny"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static("public"));
app.use(express.static("media"));
app.use(session({secret: 'ssshhhhh',saveUninitialized: true, resave: true}));

app.engine("html", require("ejs").renderFile);
app.set("view engine", "html");
app.set("views", "public");
///////////////////////////////////////////////////////////////////////////////////////////
var port = 8080;
var img_pages;
var vid_pages;
var sess;
///////////////////////////////////////////////////////////////////////////////////////////

app.get("/images/:page", function (req, res) {

    res.json({
        "page": req.params.page,
        "pages": img_pages.length,
        "files": img_pages[req.params.page-1]
    });
    readImages();
});

app.get("/videos/:page", function (req, res) {

    res.json({
        "page": req.params.page,
        "pages": vid_pages.length,
        "files": vid_pages[req.params.page-1]
    });
    readVideos();
});

app.get("/signin", function (req, res) {

    sess = req.session;
    if(sess.email)
    {
        res.redirect("/");
    }
    else{
        // res.sendFile("signin.html");
        res.render("signin.html");
    }

});

app.post("/login", function (req, res) {
    sess = req.session;
    sess.email = req.body.email;
    res.end("signed-in");
});

///////////////////////////////////////////////////////////////////////////////////////////
/// READ IMAGES AND SPLIT CONTENT
var readImages = function () {
    img_pages = [];

    fs.readdir("media/images", function (err, files) {

        if (err) throw err;
        var page;

        while (files.length) {
            page = files.splice(0, 12);
            img_pages.push(page);
        }

    });
};

var readVideos = function () {
    vid_pages = [];

    fs.readdir("media/videos", function (err, files) {

        if (err) throw err;
        var page;

        while (files.length) {
            page = files.splice(0, 12);
            vid_pages.push(page);
        }

    });

};

///////////////////////////////////////////////////////////////////////////////////////////
readImages();
readVideos();
app.listen(port);
console.log("Server running on port: " + port);
///////////////////////////////////////////////////////////////////////////////////////////