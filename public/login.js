$(function () {

    var email, pass;

    $("#loginBtn").on("click", function () {

        email = $("#email").val();
        pass = $("#password").val();

        $.post("/login", {
            "email": email,
            "pass": pass
        }, function (data) {

            if(data === "signed-in") {
                window.location.href = "/";
            }

        })

    });

});