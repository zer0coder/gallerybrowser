$(function () {

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var pagination = $("#pages");
    var imageRow = $("#imagesRow");
    var imageButton = $("#showImages");
    var videoButton = $("#showVideos");

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    imageButton.on("click", function (event) {
        event.preventDefault();
        pagination.empty();
        switchMenu();

        $.get("templates/images.hbs")
            .then(function (template) {
                var compiled = Handlebars.compile(template);

                $.getJSON("/images/1")
                    .then(function (data) {
                        var html = compiled(data);

                        imageRow.empty();
                        imageRow.append(html);
                        console.log(data);
                        createPagination(data);
                    })
            })
            .catch(function (error) {
                console.log(error);
            });
    });

    videoButton.on("click", function (event) {
        event.preventDefault();
        pagination.empty();
        switchMenu();

        $.get("templates/videos.hbs")
            .then(function (template) {
                var compiled = Handlebars.compile(template);

                $.getJSON("/videos/1")
                    .then(function (data) {

                        console.log(data);

                        var html = compiled(data);

                        imageRow.empty();
                        imageRow.append(html);
                        console.log(data);
                        createPagination(data);
                    })
            })
            .catch(function (error) {
                console.log(error);
            });
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var openPage = function () {

        var pageNum = $(this).attr("id");
        console.log(pageNum);

        $.get("templates/images.hbs")
            .then(function (template) {
                var compiled = Handlebars.compile(template);

                $.getJSON("/images/"+pageNum)
                    .then(function (data) {
                        var html = compiled(data);

                        imageRow.empty();
                        imageRow.append(html);
                        console.log(data);

                        pagination.empty();
                        createPagination(data);
                    })
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    var switchMenu = function () {

        if(!imageButton.hasClass("active")) {

            imageButton.addClass("active");
            imageButton.empty();
            imageButton.append('<i class="material-icons right">check</i>Images');
            videoButton.removeClass("active");
            videoButton.empty();
            videoButton.append("Videos");
        } else {

            videoButton.addClass("active");
            videoButton.empty();
            videoButton.append('<i class="material-icons right">check</i>Videos');
            imageButton.removeClass("active");
            imageButton.empty();
            imageButton.append("Images");
        }

    };

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    $.get("templates/images.hbs")
        .then(function (template) {
            var compiled = Handlebars.compile(template);

            $.getJSON("/images/1")
                .then(function (data) {
                    var html = compiled(data);

                    imageRow.append(html);
                    console.log(data);
                    createPagination(data);
                })
        })
        .catch(function (error) {
            console.log(error);
        });

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var createPagination = function (data) {

        var chev_left = document.createElement("li");
        var nextPage = (parseInt(data.page)+1);
        var page = parseInt(data.page);

        if( page > 1) {
            chev_left.className = "waves-effect";
            chev_left.onclick = openPage;
        } else {
            chev_left.className = "disabled";
        }
        chev_left.innerHTML = '<a href="#"><i class="material-icons">chevron_left</i></a>';
        chev_left.id = (page-1).toString();

        pagination.append(chev_left);


        for(var i = 1; i<data.pages+1; i++) {
            var li = document.createElement("li");
            li.id = i.toString();
            li.onclick = openPage;

            if(i === page) {
                li.className = "active red darken-1";
                li.innerHTML = '<a href="#">' + i + '</a>';
            } else {
                li.className = "waves-effect";
                li.innerHTML = '<a href="#">' + i + '</a>';
            }

            pagination.append(li);
        }



        var chev_right = document.createElement("li");

        if( nextPage >= data.pages+1) {
            chev_right.className = "disabled";
        } else if (page < data.pages+1) {
            chev_right.className = "waves-effect";
            chev_right.onclick = openPage;
        }
        chev_right.innerHTML = '<a href="#"><i class="material-icons">chevron_right</i></a>';
        chev_right.id = nextPage.toString();

        pagination.append(chev_right);

        $('.materialboxed').materialbox();
    };

});